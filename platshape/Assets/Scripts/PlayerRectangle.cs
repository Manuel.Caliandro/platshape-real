﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerRectangle : MonoBehaviour
{

    public float walkSpeed;
    public float jumpForce;
    public Rigidbody2D rb;
    public float Maxspeed;
    float h;
    public Transform top_left; 
    public Transform bottom_right; 
    public LayerMask ground_layers;
    public bool IsGrounded;
    public int State = 0;
    public bool Dead = false;
    public GameObject checkpoint;
    public Checkpoin checkpon;
    public Transform checkpointtransform;
    public Transform startingpoint;
    public TriggerManger camerascript;
    public GameObject cameraobject;
    public bool end = false ;
    public PlayerManager playerManager;
    public int StateWhenDead;
    public int WhatPowerUpToRespawn;
    




    void Start()
    {
        checkpointtransform = startingpoint;
    }
    void Update()
    {
      

    h = Input.GetAxis("Horizontal");
        Movement();
        Jump();
    }
    void FixedUpdate()
    {
        IsGrounded = Physics2D.OverlapArea(top_left.position, bottom_right.position, ground_layers);
    }
    void Movement()
    {
        if (h * rb.velocity.x < Maxspeed)
            rb.AddForce(Vector2.right * h * walkSpeed);

        if (Mathf.Abs(rb.velocity.x) > Maxspeed)
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * Maxspeed, rb.velocity.y);
    }
    void Jump()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded == true)
        {
            
            rb.AddForce(new Vector2(0, jumpForce));
            
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PowerStateUp")
        {
            State++;
            collision.gameObject.SetActive(false);
        }
        if (collision.gameObject.tag == "Death")
        {
            playerManager.dead = true;
            
        }
        if (collision.gameObject.tag == "Checkpoint")
        {
            checkpointtransform = collision.gameObject.GetComponent<Checkpoin>().pointTransform;
            StateWhenDead = collision.gameObject.GetComponent<Checkpoin>().playerstatesetting;
            WhatPowerUpToRespawn = collision.gameObject.GetComponent<Checkpoin>().respawnpowerup;
            Debug.Log(WhatPowerUpToRespawn);

        }
        if (collision.gameObject.tag == "MainCamera")
        {
            cameraobject.transform.position = collision.gameObject.GetComponent<TriggerManger>().pointcamera.position;
        }
        if (collision.gameObject.tag == "End")
        {
            end = true;
        }
    }

}
