﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBall : MonoBehaviour
{public float walkSpeed;
    public float jumpForce;
    public Rigidbody2D rb;
    public float Maxspeed;
    float h;
    public Transform top_left;  
    public LayerMask ground_layers;
    public bool IsGrounded;
    public float radius;
    public PlayerRectangle playerRectangle;
    public TriggerManger camerascript;
    public GameObject cameraobject;
    public PlayerManager playerManager;


    void Start()
    {
        
    }
    void Update()
    {
        if (IsGrounded == true)
        {
            h = Input.GetAxis("Horizontal");
            Movement();
        }
    }
    void FixedUpdate()
    {
        IsGrounded = Physics2D.OverlapCircle(top_left.position, radius ,ground_layers);
    }
    void Movement()
    {
        if (IsGrounded == true)
        {
            if (h * rb.velocity.x < Maxspeed  )
                rb.AddForce(Vector2.right * h * walkSpeed);

            if (Mathf.Abs(rb.velocity.x) > Maxspeed  )
                rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * Maxspeed, rb.velocity.y);
        }
      
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Death")
        {
            playerManager.dead = true;
        }
        if (collision.gameObject.tag == "Checkpoint")
        {
            playerRectangle.checkpointtransform = collision.gameObject.GetComponent<Checkpoin>().pointTransform;
            playerRectangle.StateWhenDead = collision.gameObject.GetComponent<Checkpoin>().playerstatesetting;
            playerRectangle.WhatPowerUpToRespawn = collision.gameObject.GetComponent<Checkpoin>().respawnpowerup;
        }
        if (collision.gameObject.tag == "MainCamera")
        {
            cameraobject.transform.position = collision.gameObject.GetComponent<TriggerManger>().pointcamera.position;
        }
        if (collision.gameObject.tag == "End")
        {
            playerRectangle.end = true;
        }
    }

}
