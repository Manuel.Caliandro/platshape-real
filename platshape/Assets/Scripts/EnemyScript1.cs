﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript1 : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed;
    public bool move;
    public int direction = -1;
    public int verticalDirection = 1;
    public int enemy;
    public GameObject thisgameobject;
    public bool vertical;
    public PlayerManager playerManager;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (enemy)
        {
            case 0:
                if (collision.gameObject.tag == "Rectangle")
                {
                    thisgameobject.SetActive(false);
                }
                else if (collision.gameObject.tag == "Triangle")
                {
                    playerManager.dead = true;
                }
                else if (collision.gameObject.tag == "Ball")
                {
                    playerManager.dead = true;
                }
                break;
            case 1:
                if (collision.gameObject.tag == "Rectangle")
                {
                    playerManager.dead = true;
                }
                else if (collision.gameObject.tag == "Triangle")
                {
                    thisgameobject.SetActive(false);
                }
                else if (collision.gameObject.tag == "Ball")
                {
                    playerManager.dead = true;
                }
                break;
            case 2:
                if (collision.gameObject.tag == "Rectangle")
                {
                    playerManager.dead = true;
                }
                else if (collision.gameObject.tag == "Triangle")
                {
                    playerManager.dead = true;
                }
                else if (collision.gameObject.tag == "Ball")
                {
                    thisgameobject.SetActive(false);
                }
                break;
            case 3:
                if (collision.gameObject.tag == "Rectangle")
                {
                    playerManager.dead = true;
                }
                else if (collision.gameObject.tag == "Triangle")
                {
                    playerManager.dead = true;
                }
                else if (collision.gameObject.tag == "Ball")
                {
                    playerManager.dead = true;
                }
                break;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "VerticalLimit")
        {
            verticalDirection *= -1;
            direction *= -1;

        }
    }
    void Movement()
    {
        if (move == true)
        {
            if (vertical == false)
                rb.velocity = new Vector2(speed * direction, 0);
            if (vertical == true)
                rb.velocity = new Vector2(0, speed * verticalDirection);
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }
    }
    
}

