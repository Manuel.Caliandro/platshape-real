﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTriangle : MonoBehaviour
{
    public float walkSpeed;
    public float jumpForce;
    public Rigidbody2D rb;
    public float Maxspeed;
    float h;
    public Transform top_left;
    public Transform bottom_right;
    public LayerMask ground_layers;
    public bool IsGrounded;
    public PlayerRectangle playerRectangle;
    public TriggerManger camerascript;
    public GameObject cameraobject;
    public PlayerManager playerManager;



    void Start()
    {

    }
    void Update()
    {


        h = Input.GetAxis("Horizontal");
        Movement();
        Jump();
    }
    void FixedUpdate()
    {
        IsGrounded = Physics2D.OverlapArea(top_left.position, bottom_right.position, ground_layers);
    }
    void Movement()
    {
        if (h * rb.velocity.x < Maxspeed  && IsGrounded == false)
            rb.AddForce(Vector2.right * h * walkSpeed);

        if (Mathf.Abs(rb.velocity.x) > Maxspeed && IsGrounded == false)
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * Maxspeed, rb.velocity.y);

    }
    void Jump()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded == true)
        {
            rb.AddForce(new Vector2(0, jumpForce));
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PowerStateUp")
        {
            playerRectangle.State++;
            collision.gameObject.SetActive(false);
        }
        if (collision.gameObject.tag == "Death")
        {
            playerManager.dead = true;
        }
        if (collision.gameObject.tag == "Checkpoint")
        {
            playerRectangle.checkpointtransform = collision.gameObject.GetComponent<Checkpoin>().pointTransform;
            playerRectangle.StateWhenDead = collision.gameObject.GetComponent<Checkpoin>().playerstatesetting;
            playerRectangle.WhatPowerUpToRespawn = collision.gameObject.GetComponent<Checkpoin>().respawnpowerup;
        }
        if (collision.gameObject.tag == "MainCamera")
        {
            cameraobject.transform.position = collision.gameObject.GetComponent<TriggerManger>().pointcamera.position;
        }
        if (collision.gameObject.tag == "End")
        {
            playerRectangle.end = true;
        }
    }
   
}
