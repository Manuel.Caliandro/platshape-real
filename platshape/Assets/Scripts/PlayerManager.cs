﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public GameObject[] player;
    public float BallSpawnYModifier;
    public PlayerRectangle playerRectangle;
    public bool dead = false;
    public GameObject[] enemy;
    public Transform[] enemyrespawn;
    public GameObject [] powerup;
    int respawncontroller;

    private void Start()
    {
        for (int i = 0; i < enemy.Length; i++)
        {
            enemy[i] = GameObject.Find("EnemyObject " + i);
        }
        for (int i = 0; i < enemyrespawn.Length; i++)
        {
            enemyrespawn[i] = enemy[i].transform;
        }
    }


    void Update()
    {
        Switch();
        Death();
        End(); 
        respawncontroller = playerRectangle.WhatPowerUpToRespawn;
    }

    void Switch()
    {
        if (Input.GetButtonDown("Rectagle"))
        {
            if (player[0].activeSelf == false)
            {
                if (player[1].activeSelf == true)
                {
                    player[0].transform.position = player[1].transform.position;
                    player[1].SetActive(false);
                    player[0].SetActive(true);
                }
                else
                {
                    player[0].transform.position = new Vector3(player[2].transform.position.x, player[2].transform.position.y + BallSpawnYModifier, player[2].transform.position.z);
                    player[2].SetActive(false);
                    player[0].SetActive(true);
                }
            }
        }
        if (Input.GetButtonDown("Triangle") && playerRectangle.State >0)
        {
            if (player[1].activeSelf == false)
            {
                if (player[0].activeSelf == true)
                {
                    player[1].transform.position = player[0].transform.position;
                    player[0].SetActive(false);
                    player[1].SetActive(true);
                }
                else
                {
                    player[1].transform.position = new Vector3(player[2].transform.position.x, player[2].transform.position.y + BallSpawnYModifier, player[2].transform.position.z);
                    player[2].SetActive(false);
                    player[1].SetActive(true);
                }
            }
        }
        if (Input.GetButtonDown("Ball") && playerRectangle.State > 1)
        {
            if (player[2].activeSelf == false)
            {
                if (player[0].activeSelf == true)
                {
                    player[2].transform.position = new Vector3(player[0].transform.position.x, player[0].transform.position.y - BallSpawnYModifier, player[0].transform.position.z);
                    player[0].SetActive(false);
                    player[2].SetActive(true);
                }
                else
                {
                    player[2].transform.position = new Vector3(player[1].transform.position.x, player[1].transform.position.y - BallSpawnYModifier, player[1].transform.position.z);
                    player[1].SetActive(false);
                    player[2].SetActive(true);
                }
            }
        }
    }
    void Death()
    {
        if (dead == true)
        {
            StateSetter();
            PowerupRespawn();
            enemyRespawn();
            if (player[0].activeSelf == false)
            {
                if (player[1].activeSelf == true)
                {
                    player[1].SetActive(false);
                }
                else
                {
                    player[2].SetActive(false);
                }
                player[0].SetActive(true);
            } 
            player[0].transform.position = playerRectangle.checkpointtransform.position;
            player[0].GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
            dead = false;
        }
       
    }
    void End()
    {
        if (playerRectangle.end == true)
        {
            SceneManager.LoadScene("Scene 2");
        }
    }
    void enemyRespawn()
    {
        for (int i = 0; i < enemy.Length; i++)
        {
            enemy[i].transform.position = enemyrespawn[i].position;
            if (enemy[i].activeSelf == false)
            {
                enemy[i].SetActive(true);
            }
        }
    }
    void PowerupRespawn()
    {
        
        if (respawncontroller == 0)
        { 
            powerup[0].SetActive(true);
            powerup[1].SetActive(true);
            Debug.Log("hei");
        }
        if (respawncontroller == 1)
        {
            powerup[0].SetActive(false);
            powerup[1].SetActive(true);
        }
    }
    void StateSetter()
    {
        playerRectangle.State = playerRectangle.StateWhenDead;
    }
}

